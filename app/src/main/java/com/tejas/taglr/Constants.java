package com.tejas.taglr;

/*
 * Created by tejasj on 24-09-2016.
 */
public class Constants {

    public static final String BOOK_API_TEST = "https://api.nytimes.com/svc/books/v3/lists.json?api-key=969baed396594c61aa7145f9ae0d1911&list=e-book-fiction";
    public static final String BOOK_API = "https://api.nytimes.com/svc/books/v3/lists.json";
    public static final String API_KEY = "api-key";
    public static final String LIST_KEY = "list";
    public static final String MOVIE_API_TEST = "https://api.nytimes.com/svc/movies/v2/critics/all.json?api-key=969baed396594c61aa7145f9ae0d1911";

    public static final String MOVIE_REVIEW_API = "https://api.nytimes.com/svc/movies/v2/critics/all.json";

    public static final String MOVIE_API_2_TEST ="https://api.nytimes.com/svc/movies/v2/reviews/search.json?api-key=969baed396594c61aa7145f9ae0d1911";
    public static final String MOVIE_API_3_TEST ="https://api.nytimes.com/svc/movies/v2/reviews/search.json?api-key=969baed396594c61aa7145f9ae0d1911&reviewer=Manohla Dargis";


    public static final String TOP_STORIES_API_TEST = "https://api.nytimes.com/svc/topstories/v1/home.json?api-key=969baed396594c61aa7145f9ae0d1911";

    public static final String DISPLAY_NAME = "display_name";
    public static final String DISPLAY_TITLE = "display_title";
    public static final String SUMMARY_SHORT = "summary_short";
    public static final String BIO = "bio";
    public static final String MULTIMEDIA = "multimedia";
    public static final String SRC = "src";
    public static final String RESOURCE = "resource";
    public static final String BOOK_DETAILS = "book_details";
    public static final String DESCRIPTION = "description";
    public static final String TITLE = "title";
    public static final String STATUS = "status";
    public static final String OK = "OK";
    public static final String RESULTS = "results";



    ///display_name
    //
    ///
    //
}
