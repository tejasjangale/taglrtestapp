package com.tejas.taglr;

/***
 * Created by tejasj on 25-09-2016.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class NetworkUtils {


    public static String performNetworkCall(String strUrl, String strRequest) throws IOException {
        InputStream inputStream = getResponse(strUrl, strRequest);
        return getStringResponse(inputStream);
    }


    public static String performNetworkCall(String strUrl, HashMap<String, String> params) throws IOException {
        InputStream inputStream = getResponsePost(strUrl, params);
        return getStringResponse(inputStream);
    }


    private static InputStream getResponse(String strUrl, String strRequest) throws IOException {

        URL url = null;
        try {
            url = new URL(strUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("authkey", "25764f2857672768553b442c352c392a6c497a5f30516b235b7b7c3336");
        conn.setRequestProperty("Content-Type", "application/json");
//        String strRequest = getPostDataString(params);
        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(strRequest);
        writer.flush();
        writer.close();
        os.close();

        conn.connect();
        return conn.getInputStream();
    }

    private static InputStream getResponsePost(String strUrl, HashMap<String, String> params) throws IOException {

        URL url = null;
        try {
            url = new URL(strUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
//        conn.setRequestProperty("authkey", "25764f2857672768553b442c352c392a6c497a5f30516b235b7b7c3336");
//        conn.setRequestProperty("Content-Type", "application/json");
        String strRequest = getPostDataString(params);
        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(strRequest);
        writer.flush();
        writer.close();
        os.close();

        conn.connect();
        return conn.getInputStream();
    }

    private static InputStream getResponsejava(String strUrl, String strRequest) throws IOException {


        URL url = null;
        try {
            url = new URL(strUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("authkey", "25764f2857672768553b442c352c392a6c497a5f30516b235b7b7c3336");
        conn.setRequestProperty("Content-Type", "application/json");
//        String strRequest = getPostDataString(params);
        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(strRequest);
        writer.flush();
        writer.close();
        os.close();

        conn.connect();
        return conn.getInputStream();
    }

    // HTTP GET request
    public static String sendGet(String url) throws IOException {


        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        boolean b = con.getUseCaches();


        con.setUseCaches(true);
//        con.addRequestProperty("Cache-Control", "max-age=600000");

        //add request header
//        con.setRequestProperty("authkey", "25764f2857672768553b442c352c392a6c497a5f30516b235b7b7c3336");
//        con.setRequestProperty("Content-Type", "application/json");
        int responseCode = con.getResponseCode();
//        System.out.println("\nSending 'GET' request to URL : " + url);
//        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
        return response.toString();
    }

    private static String getStringResponse(InputStream inputStream) {
        String result = "";
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine;
        try {
            while ((inputLine = in.readLine()) != null) {
                result += inputLine;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}

