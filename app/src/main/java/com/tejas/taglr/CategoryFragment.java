package com.tejas.taglr;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by tejasj on 25-09-2016.
 */

/**
 * A placeholder fragment containing a simple view.
 */
public class CategoryFragment extends Fragment implements View.OnClickListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private Context mContext;
    private Activity mActivity;
    private Button mRefreshButton;
    private CategoryListAdapter mCategoryListAdapter;

    public CategoryFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
        mActivity = activity;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static CategoryFragment newInstance(int sectionNumber) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_tab, container, false);
        mRefreshButton = (Button) rootView.findViewById(R.id.refreshButton);
        int currentTab = getArguments().getInt(ARG_SECTION_NUMBER);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);

        mCategoryListAdapter = new CategoryListAdapter(new ArrayList<CategoryItem>());
        recyclerView.setAdapter(mCategoryListAdapter);
        mRefreshButton.setOnClickListener(this);
        mRefreshButton.setTag(currentTab);
        getData(currentTab);
        return rootView;
    }

    private void getData(int currentTab) {
        switch (currentTab) {

            case 1: {
                getCategoryData();
                break;
            }
            case 2: {
                getMoviewCritic();
                break;
            }
            case 3: {
//                getTopStories(mCategoryListAdapter);
                getBooks();
                break;
            }

            default:
                getCategoryData();
        }

    }

    private void getMoviewCritic() {

        ApiCallTask myAsyncTask = new ApiCallTask(mActivity, new onTaskCompletedListener() {
            @Override
            public void onTaskCompleted(String result) {
                Log.e("getMoviewCritic", result);
                ArrayList<CategoryItem> categoryItems = new ArrayList<>();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.getString(Constants.STATUS);
                    if (status.equalsIgnoreCase(Constants.OK)) {
                        JSONArray results = jsonObject.getJSONArray(Constants.RESULTS);
                        for (int i = 0; i < results.length(); i++) {
                            JSONObject jsonObject1 = (JSONObject) results.get(i);
//                        String name;
                            String title;
                            String description;
                            String imageurl = "";
                            title = jsonObject1.getString(Constants.DISPLAY_NAME);
                            description = jsonObject1.getString(Constants.BIO);
                            try {
                                JSONObject resouceObject = jsonObject1.getJSONObject(Constants.MULTIMEDIA).getJSONObject(Constants.RESOURCE);
                                try {
                                    imageurl = resouceObject.getString(Constants.SRC);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
//                                resouceObject.getJSONObject("resource")


                            } catch (JSONException e) {
                                e.printStackTrace();
                                imageurl = "";
                            }


                            categoryItems.add(new CategoryItem(title, description, imageurl));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mCategoryListAdapter.setData(categoryItems);
                mCategoryListAdapter.notifyDataSetChanged();
            }

        });

        myAsyncTask.execute(Constants.MOVIE_API_TEST);
    }

    private void getCategoryData() {

        ApiCallTask myAsyncTask = new ApiCallTask(mActivity, new onTaskCompletedListener() {
            @Override
            public void onTaskCompleted(String result) {
                Log.e("getCategoryData", result);
                if (result != null) {
                    ArrayList<CategoryItem> categoryItems = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String status = jsonObject.getString(Constants.STATUS);
                        if (status.equalsIgnoreCase(Constants.OK)) {
                            JSONArray results = jsonObject.getJSONArray(Constants.RESULTS);
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject jsonObject1 = (JSONObject) results.get(i);
//                        String name;
                                String title;
                                String description;
                                String imageurl = "";
                                title = jsonObject1.getString(Constants.DISPLAY_TITLE);
                                ///display_name
                                //display_title
                                ///summary_short
                                //bio
                                description = jsonObject1.getString(Constants.SUMMARY_SHORT);
                                try {
                                    JSONObject resouceObject = jsonObject1.getJSONObject(Constants.MULTIMEDIA);
                                    try {
                                        imageurl = resouceObject.getString(Constants.SRC);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
//                                resouceObject.getJSONObject("resource")


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    imageurl = "";
                                }


                                categoryItems.add(new CategoryItem(title, description, imageurl));
                            }
                            mCategoryListAdapter.setData(categoryItems);
                            mCategoryListAdapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

        });

        myAsyncTask.execute(Constants.MOVIE_API_2_TEST);
    }

    private void getBooks() {

        ApiCallTask myAsyncTask = new ApiCallTask(mActivity, new onTaskCompletedListener() {
            @Override
            public void onTaskCompleted(String result) {

                Log.e("getCategoryData", result);
                if (result != null) {
                    ArrayList<CategoryItem> categoryItems = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        String status = jsonObject.getString(Constants.STATUS);
                        if (status.equalsIgnoreCase(Constants.OK)) {
                            JSONArray results = jsonObject.getJSONArray(Constants.RESULTS);
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject jsonObject1 = (JSONObject) results.get(i);

                                JSONArray bookitem = jsonObject1.getJSONArray(Constants.BOOK_DETAILS);
                                JSONObject jsonBook = bookitem.getJSONObject(0);
                                //book_details
//                        String name;
                                String title;
                                String description;
                                String imageurl = "";
                                title = jsonBook.getString(Constants.TITLE);
                                ///display_name
                                //display_title
                                ///summary_short
                                //bio
                                description = jsonBook.getString(Constants.DESCRIPTION);


                                categoryItems.add(new CategoryItem(title, description, imageurl));
                            }
                            mCategoryListAdapter.setData(categoryItems);
                            mCategoryListAdapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

        });

        myAsyncTask.execute(Constants.BOOK_API_TEST);
    }

    private void getTopStories(final CategoryListAdapter categoryListAdapter) {

        ApiCallTask myAsyncTask = new ApiCallTask(mActivity, new onTaskCompletedListener() {
            @Override
            public void onTaskCompleted(String result) {
                Log.e("getTopStories", result);
                ArrayList<CategoryItem> categoryItems = new ArrayList<>();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("OK")) {
                        JSONArray results = jsonObject.getJSONArray("results");
                        for (int i = 0; i < results.length(); i++) {
                            JSONObject jsonObject1 = (JSONObject) results.get(i);
                            String title;
                            String description;
                            String imageurl = "";
                            title = jsonObject1.getString("title");
                            description = jsonObject1.getString("abstract");
                            try {
                                JSONArray resouceObject = jsonObject1.getJSONArray("multimedia");
                                try {
                                    imageurl = resouceObject.getJSONObject(0).getString("url");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
//                                resouceObject.getJSONObject("resource")


                            } catch (JSONException e) {
                                e.printStackTrace();
                                imageurl = "";
                            }


                            categoryItems.add(new CategoryItem(title, description, imageurl));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mCategoryListAdapter.setData(categoryItems);
                mCategoryListAdapter.notifyDataSetChanged();
            }

        });

        myAsyncTask.execute(Constants.TOP_STORIES_API_TEST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.refreshButton:
                int currentTab = (int) v.getTag();
                getData(currentTab);
        }
    }

    public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

        private List<CategoryItem> categoryItems;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageView;
            private TextView subCatTextView;
            private TextView dateTextView;

            public MyViewHolder(View view) {
                super(view);

                imageView = (ImageView) view.findViewById(R.id.imageView);
                subCatTextView = (TextView) view.findViewById(R.id.titleText);
                dateTextView = (TextView) view.findViewById(R.id.desciptionText);
            }
        }

        public CategoryListAdapter(List<CategoryItem> categoryItems) {
            this.categoryItems = categoryItems;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.categoryitem, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            CategoryItem item = categoryItems.get(position);
            holder.subCatTextView.setText(item.getTitle());
            holder.dateTextView.setText(item.getDesciption());
//            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ENGLISH);
            String internetUrl = item.getImageUrl();
                Glide.with(mContext)
                        .load(internetUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

//                                holder.imageView.setBackground(resource);
                                return false;
                            }
                        })
                        .into(holder.imageView);
        }

        @Override
        public int getItemCount() {
            return categoryItems.size();
        }

        public void setData(List<CategoryItem> list) {
            if (list.size() == 0) {
                mRefreshButton.setVisibility(View.VISIBLE);
            } else {
                mRefreshButton.setVisibility(View.GONE);
            }
            categoryItems = list;
            notifyDataSetChanged();
        }

      /*  public List<CategoryItem> getData() {
            return categoryItems;
        }*/
    }
}
