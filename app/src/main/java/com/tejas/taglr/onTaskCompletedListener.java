package com.tejas.taglr;

/**
 * Created by tejasj on 25-09-2016.
 */
public interface onTaskCompletedListener {
    void onTaskCompleted(String result);

}
