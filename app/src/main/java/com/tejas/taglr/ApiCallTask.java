package com.tejas.taglr;

/*
 * Created by tejasj on 25-09-2016.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import java.io.IOException;

public class ApiCallTask extends AsyncTask<String, Void, String> {
    onTaskCompletedListener listener;
    String request = "";
    Activity activity;

    public ApiCallTask(Activity activity, onTaskCompletedListener listener) {
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... strings) {
        String result = null;

//        if (Utility.isConnected(activity.getApplicationContext())) {
        try {
            result = NetworkUtils.sendGet(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
            result = null;
        }
//        }
//    else {
//            result = activity.getResources().getString(R.string.no_internet_connection);
//        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {

            if (result.equalsIgnoreCase(activity.getResources().getString(R.string.no_internet_connection))) {
                showAlertDialog(activity.getResources().getString(R.string.no_internet_connection), activity);
            }
            listener.onTaskCompleted(result);

        } else {
            if (!Utility.isConnected(activity.getApplicationContext())) {
                result = activity.getResources().getString(R.string.no_internet_connection);
                showAlertDialog(activity.getResources().getString(R.string.no_internet_connection), activity);

                listener.onTaskCompleted(result);

            } else {
                listener.onTaskCompleted(result);
                showAlertDialog(activity.getResources().getString(R.string.msg_something_went_wrong), activity);

            }

        }
    }

    private void showAlertDialog(String msg, Activity activity) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(activity, R.style.AlertDialogStyle);
        builder.setTitle(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }
}
