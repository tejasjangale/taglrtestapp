package com.tejas.taglr;

/**
 * Created by tejasj on 25-09-2016.
 */
public class CategoryItem {

    String title;
    String desciption;
    String imageUrl;

    public CategoryItem(String title, String desciption, String imageUrl ){

        this.title = title;
        this.desciption = desciption;
        this.imageUrl = imageUrl;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }
    public void setDesciption(String desciption){
        this.desciption = desciption;
    }

    public String getDesciption(){
        return desciption;
    }
    public void setImageUrl(String imageUrl){
        this.imageUrl = imageUrl;
    }

    public String getImageUrl(){
        return imageUrl;
    }
}
